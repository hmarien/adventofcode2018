module Day2p2 where

import Data.Function
import Data.List hiding (insert)
import Data.Bifunctor
import Data.Set hiding (map)

--given an input
--split into lines
--duplicate each line n times, each time ommitting 1 character
--transpose into a list of columns
--find duplicate
--display result
day2p2 :: IO ()
day2p2 = do
  input <- readFile "../resources/Day2/input"
  input & lines & transpose & duplc & fmap transpose & map findDuplicates & show & putStrLn

--duplicate each line n times, each time ommitting 1 character
--eg "abcd" -> ["abc","abd","acd","bcd"]
duplc :: [a] -> [[a]]
duplc [] = [  ]
duplc (xs) = go [] [] xs where
  go _     acc []     = acc
  go front acc (x:xs) = go (front ++ [x]) ((front++xs):acc) xs

--find all duplicate strings
findDuplicates :: [String] -> [String]
findDuplicates = go empty [] where
  go _   acc []     = acc
  go set acc (x:xs) = go (insert x set) (if member x set then x:acc else acc) xs
