module Day4p1 where

import Text.Parsec.String
import Text.Parsec
import Data.Char
import Data.Either
import Text.ParserCombinators.Parsec.Char
import Data.Function

day4p1 :: IO ()
day4p1 = undefined
--do
--  input <- readFile "../resources/Day4/input"
--  input & lines & map (runParsec timeEntry) & map (fromRight ((0,0,0),FallAsleep)) & show & putStrLn

type Datum = (Int, Int, Int)
data Action = FallAsleep | WakesUp | BeginsShift Int deriving (Show)


-------------------------
--    PARSER STUFF     --
-------------------------

datum :: Parser Datum
datum = do
  char '['
  year <- count 4 digit
  char '-'
  month <- count 2 digit
  char '-'
  day <- count 2 digit
  space
  hour <- count 2 digit
  char ':'
  min <- count 2 digit
  char ']'
  return (read year, read month, read day)

action :: Parser Action
action = undefined

timeEntry :: Parser (Datum, Action) 
timeEntry = do
  dat <- datum
  space
  act <- action
  return (dat, act)

runParsec :: Parser a -> String -> Either ParseError a
runParsec p i = runParser p () "" i
