module Day1p2 where

import Text.Parsec.String
import Text.Parsec
import Data.Char
import Data.Either
import Text.ParserCombinators.Parsec.Char
import Data.Functor.Foldable
import Data.Function
import Data.IntSet


--take input
--devide in lines
--parse each line
--cycle result
--produce an list that accumulates results
--find the first duplicate
--display
day1p2 :: IO ()
day1p2 = do
  input <- readFile "../resources/Day1/input"
  input & lines & fmap (runParsec number) & fmap (fromRight 0) & cycle & accumulationList & findFirstDuplicate & show & putStrLn


--each element in the result is the summation of all previous elements eg [1,2,3,4] -> [1,3,6,10] 
accumulationList :: Num a => [a] -> [a]
accumulationList = go 0 where
  go acc [] = []
  go acc (x:xs) = (x+acc) : go (x+acc) xs

--returns the first duplicate element in a list eg [1,2,3,4,2,1] -> Just 2
findFirstDuplicate :: [Int] -> Maybe Int
findFirstDuplicate = go empty where
  go set []     = Nothing
  go set (x:xs) = if member x set then Just x else go (insert x set) xs


-------------------------
--    PARSER STUFF     --
-------------------------

--Parse either '+' or '-' and returns +1 or -1
sign :: Parser Int
sign = do
  s <- char '-' <|> char '+'
  return (case s of
    '+' -> 1
    '-' -> (-1) )

--parse a number with sign
number :: Parser Int
number = do
  s <- sign
  i <- many1 alphaNum
  return $ s*(read i)

--run parser
runParsec :: Parser a -> String -> Either ParseError a
runParsec p i = runParser p () "" i


