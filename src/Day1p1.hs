module Day1p1 where

import Text.Parsec.String
import Text.Parsec
import Data.Char
import Data.Either
import Text.ParserCombinators.Parsec.Char

--takes input
--split in lines
--parse each line
--add the results together
--display result
day1p1 :: IO ()
day1p1 = do
  input <- readFile "../resources/Day1/input"
  putStrLn $ show . sum . map (fromRight 0) . map (runParsec number) . lines $ input


-------------------------
--    PARSER STUFF     --
-------------------------

--Parse either '+' or '-' and returns +1 or -1
sign :: Parser Int
sign = do
  s <- char '-' <|> char '+'
  return (case s of
    '+' -> 1
    '-' -> (-1) )

--parse a number with sign
number :: Parser Int
number = do
  s <- sign
  i <- many1 digit
  return $ s*(read i)

--run parser
runParsec :: Parser a -> String -> Either ParseError a
runParsec p i = runParser p () "" i
