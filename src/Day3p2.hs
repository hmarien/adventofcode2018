module Day3p2 where

import Text.Parsec.String
import Text.Parsec
import Data.Char
import Data.Either
import Data.List hiding (insert)
import Data.Function
import Text.ParserCombinators.Parsec.Char
import Data.Set hiding (take)

--given an input
--parse each line
--find all rectangles with no overlap
--get id of these rectangles
--display resultss
day3p2 :: IO ()
day3p2 = do
  input <- readFile "../resources/Day3/input"
  input & lines & fmap (runParsec datum) & fmap (fromRight (0,(0,0),(0,0))) & noDuplicates & fmap getId & show & putStrLn

-- (ID, (X-coordinate, y-coordinate), (width, height))
type Datum = (Int, (Int,Int), (Int,Int))

--get rectangle ID
getId :: Datum -> Int
getId (id, _, _) = id


--accumulates all rectangles that have no overlap with any other rectangle
noOverlap :: [Datum] -> [Datum]
noOverlap [] = []
noOverlap ks = go [] ks ks where
  go :: [Datum] -> [Datum] -> [Datum] -> [Datum]
  go acc ds []     = acc
  go acc ds (x:xs) = if overlapsAny x ds then go acc ds xs else go (x:acc) ds xs

--detects if 1 rectangles overlaps with any rectangle in the list
overlapsAny :: Datum -> [Datum] -> Bool
overlapsAny d [] = False
overlapsAny d (x:xs) = if overlaps d x then True else overlapsAny d xs


findSingleCoordinates :: [(Int, Int)] -> [(Int, Int)]
findSingleCoordinates = (go empty empty) where
  go :: Set (Int,Int) -> Set (Int, Int) -> [(Int, Int)] -> [(Int, Int)]
  go acc dupl []     = toList dupl
  go acc dupl (x:xs) = go (if member x acc then acc else insert x acc) (if member x acc then insert x dupl else dupl) xs

--lists all covered coordinates for 1 rectangle
coordinates :: Datum -> [(Int,Int)]
coordinates (_, (x,y), (w,h)) = [(x+dx, y+dy) | dx <- [0..(w-1)], dy <- [0..(h-1)]]

--detects overlap between 2 rectangles
overlaps :: Datum -> Datum -> Bool
overlaps a b = result where
  (id1, (x1,y1), (w1,h1)) = a
  (id2, (x2,y2), (w2,h2)) = b
  result = and [
    x1 < x2 + w2,
    x1 + w1 > x2,
    y1 < y2 + h2,
    y1 + h1 > y2,
    id1 /= id2]

-------------------------
--    PARSER STUFF     --
-------------------------

--parse 1 line into data
datum :: Parser Datum
datum = do
  char '#'
  id <- many1 digit
  space
  char '@'
  space
  x <- many1 digit
  char ','
  y <- many1 digit
  char ':'
  space
  w <- many1 digit
  char 'x'
  h <- many1 digit
  return (read id, (read x, read y), (read w, read h))

--run parser
runParsec :: Parser a -> String -> Either ParseError a
runParsec p i = runParser p () "" i
