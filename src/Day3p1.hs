module Day3p1 where

import Text.Parsec.String
import Text.Parsec
import Data.Char
import Data.Either
import Data.List hiding (insert)
import Data.Function
import Text.ParserCombinators.Parsec.Char
import Data.Set


--given an input
--parse each line
--extract coordinates from each rectangles
--combine all coordinates into a list (can contain duplicates)
--find duplicates
--display result
day3p1 :: IO ()
day3p1 = do
  input <- readFile "../resources/Day3/input"
  input & lines & fmap (runParsec datum) & fmap (fromRight (0,(0,0),(0,0))) & fmap coordinates & concat & findNumberOfDuplicates & show & putStrLn

-- (ID, (X-coordinate, y-coordinate), (width, height))
type Datum = (Int, (Int,Int), (Int,Int))

--finds in a lsit of coorinates the number of coordinates that is overlapped by 2 or more rectangles
findNumberOfDuplicates :: [(Int, Int)] -> Int
findNumberOfDuplicates = size . (go empty empty) where
  --acc is an accumulator that accumulates unique coordinates
  --duplc is an accumulator that accumulates duplicate coordinates
  go :: Set (Int,Int) -> Set (Int, Int) -> [(Int, Int)] -> Set (Int, Int)
  go acc dupl []     = dupl
  go acc dupl (x:xs) = go (if member x acc then acc else insert x acc) (if member x acc then insert x dupl else dupl) xs

--lists all covered coordinates for 1 rectangle
coordinates :: Datum -> [(Int,Int)]
coordinates (_, (x,y), (w,h)) = [(x+dx, y+dy) | dx <- [0..(w-1)], dy <- [0..(h-1)]]

--detects overlap between 2 rectangles
overlaps :: Datum -> Datum -> Bool
overlaps a b = result where
  (_, (x1,y1), (w1,h1)) = a
  (_, (x2,y2), (w2,h2)) = b
  result = and [
    x1 < x2 + w2,
    x1 + w1 > x2,
    y1 < y2 + h2,
    y1 + h1 > y2]

-------------------------
--    PARSER STUFF     --
-------------------------

--parse 1 line into data
datum :: Parser Datum
datum = do
  char '#'
  id <- many1 digit
  space
  char '@'
  space
  x <- many1 digit
  char ','
  y <- many1 digit
  char ':'
  space
  w <- many1 digit
  char 'x'
  h <- many1 digit
  return (read id, (read x, read y), (read w, read h))

--run parser
runParsec :: Parser a -> String -> Either ParseError a
runParsec p i = runParser p () "" i
