module Day2p1 where

import Data.Function
import Data.List
import Data.Bifunctor


--given an input
--divide into lines
--count character occurences in each line
--detect double or triple occurences in each line
--count total tupes and triples in the list
--multiply number of doubles and number of triples
--display result
day2p1 :: IO ()
day2p1 = do
  input <- readFile "../resources/Day2/input"
  input & lines & fmap convertToCounts & fmap detectTupleTriple & countTupleTriple & uncurry (*) & show & putStrLn

--given an input string
--produces number of occurences for each character eg "aabbdea" -> [(3,'a'), (2,'b'), (1,'d'), (1,'e')]
convertToCounts :: String -> [(Int, Char)]
convertToCounts [] = []
convertToCounts (c:cs) = (count, c) : convertToCounts xs
  where
    (occurences, xs) = partition (== c) cs
    count = length occurences + 1

--Detects if string has a character which occurs 2 or 3 times
detectTupleTriple :: [(Int, Char)] -> (Bool, Bool)
detectTupleTriple = ho . unzip . fmap has2or3

--detects if a character has exactly 2 or 3 occurences
has2or3 :: (Int, Char) -> (Bool, Bool)
has2or3 (x,_) = case x of
  2         -> (True, False)
  3         -> (False, True)
  otherwise -> (False, False)

ho :: ([Bool],[Bool]) -> (Bool, Bool)
ho = bimap or or

--count how many 
countTupleTriple :: [(Bool, Bool)] -> (Int, Int)
countTupleTriple = foldr go (0,0) where
  go x (two's, threes) = (newFst, newSnd) where
    newFst = two's  + if fst x then 1 else 0
    newSnd = threes + if snd x then 1 else 0
