module Main where

import Day1p1
import Day1p2

import Day2p1
import Day2p2

import Day3p1
import Day3p2

import Day4p1

main :: IO ()
main = do
  day4p1
